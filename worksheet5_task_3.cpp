#include<iostream>
using namespace std;

struct Node
{
    int data;
    struct Node *next;
};

//insert a new node infront of the list
void push (struct Node** head, int node_data)
{
    //1.create and allocate node
    struct Node* newNode = new Node;
    //2. Assign data to node
    newNode->data = node_data;
    //3. Set next of newnode as head
    newNode->next = (*head);
    //4.Move the head to point to the newNode
    (*head) = newNode;
}

//Insert new node after a given node
void insertAfter(struct Node* prev_node, int node_data)
{
    /*Check if the given prev_node is NULL*/
    if(prev_node == NULL)
    {
        cout<<"The given previous node is required, cannot be NULL"; return;
    }
    /*create and allocate new node*/
    struct Node* newNode = new Node;
    /*assign data to the node*/
    newNode->data = node_data;
    /*Make next of new ndoe as next of prev_node*/
    newNode->next = prev_node->next;
    /*move the next of prev_node as new_node*/
    prev_node->next = newNode;
}

/*insert new node at the end of the linked list*/
void append(struct Node** head, int node_data)
{
    /*Create and allocate node*/
    struct Node* newNode = new Node;
    struct Node* last = *head; /*used in step 5*/
    /*2.Assign data to the node*/
    newNode->data = node_data;
    /*3.Set next pointer of new node to null as its the last node*/
    newNode->next = NULL;
    /*4.if list is empty, new nodes becomes the first node*/
    if(*head == NULL)
    {
        *head = newNode;
        return;
    }
    /*5.Else traverse till the last node*/
    while(last->next != NULL)
    last = last->next;
    /*Changes the next of last nodes*/
    last->next = newNode;
    return;
}

//display linked list contents
void displaylist(struct Node* node)
{
    //traverse the list to display each node
    while(node != NULL)
    {
        cout<<node->data<<"-->";
        node = node->next;
    }
    if(node == NULL)
    cout<<"NULL";
}

/*main program for linked list*/
int main(){
    /*empty list*/
    struct Node* head = NULL;
    //insert 10
    append(&head, 10);
    //insert 20 at the begining
    push(&head, 20);
    //insert 30 at the begining
    push(&head, 30);
    //insert 40, at the end
    append(&head, 40);
    //insert 50, after 20.
    insertAfter(head->next,50);
    cout<<"linked list"<<endl;
    displaylist(head);
    return 0;
}

