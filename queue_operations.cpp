#include<iostream>
using namespace std;
#define MAX_SIZE 5
int queue_array[MAX_SIZE];
int front_pos =-1;
int rear_pos =-1;

bool is_queue_empty()
{
    if(front_pos==-1 || front_pos==rear_pos+1 )
    return true;
    else
    return false;
}

bool is_queue_full()
{
    if(front_pos==MAX_SIZE-1)
    return true;
    else
    return false;
}

void enqueue(int data)
{
    if(is_queue_full())
    {
        cout<<"Queue is Overflow";
        return;
    }
    if (front_pos ==-1)
    front_pos=0;
    rear_pos++;
    queue_array[rear_pos]=data;
}

int dequeue()
{
    if(is_queue_empty())
    {
        cout<<"Queue is Underflow";
        return -1;
    }
    int value;
    value=queue_array[front_pos];
    front_pos++;
    return value;
}

int peek()
{
    if (is_queue_empty())
    {
        cout<<"Queue is Underflow";
        return -1;
    }
    else
   return queue_array[front_pos];
}

void display()
{
    if(is_queue_empty())
    {
        cout<<"Queue is UnderFlow";
        return;
    }
    int i;
    cout<<"Queue elements are: ";
    for(i=front_pos; i<=rear_pos; i++)
    {
        cout<<queue_array[i]<<" ";
    }
    cout<<endl;
}

int main()
{
    int data, top;
    enqueue(124);
    enqueue(152);
    display();
    data=dequeue();
    cout<<"deleted element: "<<data<<endl;
    cout<<"after deleting element: "<<endl;
    display();
    top=peek();
    cout<<"The top most element: "<<top<<endl;
    return 0;
}
