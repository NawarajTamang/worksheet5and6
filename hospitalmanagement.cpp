#include <iostream>
#include <queue>
#include <map>
#include <string>
#include <ctime>

using namespace std;

class Patient
{
public:
    string patientid;
    long long social_id;
    string occupation;
    string disease_reported;
    string treatment_prescribed;
    string previous_treated;
    int token_number;
    tm dateofVisit;
    int department;
};

class Hospital
{
private:
    queue<Patient> dermatology_queue;
    queue<Patient> cardiology_queue;
    queue<Patient> gynecology_queue;
    queue<Patient> pathology_queue;
    map<int, Patient> patient_map;
    int token_counter; // to keep track of last assigned token

public:
    void assign_token(Patient &p)
    {
        p.token_number = token_counter++;
        patient_map[p.token_number] = p;
    }

    void add_to_queue(Patient p, int department)
    {
        assign_token(p);
        if (department == 0)
            dermatology_queue.push(p);
        else if (department == 1)
            cardiology_queue.push(p);
        else if (department == 2)
            gynecology_queue.push(p);
        else if (department == 3)
            pathology_queue.push(p);
    }

    int total_in_queue(int department)
    {
        if (department == 0)
            return dermatology_queue.size();
        else if (department == 1)
            return cardiology_queue.size();
        else if (department == 2)
            return gynecology_queue.size();
        else if (department == 3)
            return pathology_queue.size();
        else
            return -1;
    }

    int total_waiting()
    {
        int total = 0;
        total += total_in_queue(0);
        total += total_in_queue(1);
        total += total_in_queue(2);
        total += total_in_queue(3);
        return total;
    }

    int average_arrival()
    {
        // logic to calculate average number of patients arriving per day
        // using data collected over a week or month
        return -1;
    }

    void remove_from_queue(int token_number)
    {
        Patient p = patient_map[token_number];
        if (p.department == 0)
            dermatology_queue.pop();
        else if (p.department == 1)
            cardiology_queue.pop();
        else if (p.department == 2)
            gynecology_queue.pop();
        else if (p.department == 3)
            pathology_queue.pop();
        patient_map.erase(token_number);
    }

    void update_patient_info(int token_number, Patient new_info)
    {
        patient_map[token_number] = new_info;
    }

    Patient get_next_patient(int department)
    {
        if (department == 0)
            return dermatology_queue.front();
        else if (department == 1)
            return cardiology_queue.front();
        else if (department == 2)
            return gynecology_queue.front();
        else if (department == 3)
            return pathology_queue.front();
    }
};

int main()
{
    Patient p;
    Hospital h;
    int department;
    // initialize patient information
    cout << "Enter Patient ID: ";
    cin >> p.patientid;
    cout << "Enter Social ID: ";
    cin >> p.social_id;
    cout << "Enter Occupation: ";
    cin >> p.occupation;
    cout << "Enter Disease Reported: ";
    cin >> p.disease_reported;
    cout << "Enter Treatment Prescribed: ";
    cin >> p.treatment_prescribed;
    cout << "Enter Previous Treated (Yes/No): ";
    cin >> p.previous_treated;
    cout << "Enter Department: ";
    cin >> department;
    h.add_to_queue(p, department);

    // if(p.department == "dermatology")
    //     add_to_queue(p, dermatology_queue);
    // else if(p.department == "cardiology")
    //     add_to_queue(p, cardiology_queue);
    // else if(p.department == "gynecology")
    //     add_to_queue(p, gynecology_queue);
    // else if(p.department == "pathology")
    //     add_to_queue(p, pathology_queue);
    // // other departments as needed

    cout << "Total in queue: " << h.total_in_queue(department) << endl;
    cout << "Total waiting: " << h.total_waiting() << endl;
    cout << "Average arrival: " << h.average_arrival() << endl;

    return 0;
}
