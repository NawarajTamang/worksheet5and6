#include<iostream>
using namespace std; 
struct Node{
    int data;
    struct Node* next;
};
struct Node* head_ptr=NULL;

void Insert_node(int data)
{
    struct Node* new_node = new Node;
    new_node->data = data;
    new_node->next = head_ptr;
    head_ptr = new_node;
}

void Display_list()
{
    struct Node* ptr = head_ptr;
    while(ptr != NULL)
    {
        cout << ptr->data << " ";
        ptr = ptr->next;
    }
}

void Reverse_list()
{
    struct Node* prev = NULL;
    struct Node* current = head_ptr;
    struct Node* next = NULL;
    while(current != NULL)
    {
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    head_ptr = prev;
}

int main()
{
    Insert_node(12);
    Insert_node(13);
    Insert_node(14);
    Insert_node(15);
    cout << "Before reversing List: ";
    Display_list();
    cout << endl;
    Reverse_list();
    cout << "After reversing List: ";
    Display_list();
    struct Node* ptr = head_ptr;
    while(ptr != NULL)
    {
        struct Node* temp = ptr;
        ptr = ptr->next;
        delete temp;
    }
    return 0;
}
