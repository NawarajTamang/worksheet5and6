#include<iostream>
#include<fstream>
#include<string>
using namespace std;

struct Employee
{
    string name;
    int tax_number;
    int salary;
};

int main()
{
    Employee employee[100];
    int i, salary_threshold, count = 0;
    double avg_salary = 0;

    // Read test.bin file
    ifstream infile;
    infile.open("test.bin", ios::binary);
    if(infile) {
        infile.read((char*)&employee, sizeof(employee));
    }
    infile.close();

    // Prompt user for salary threshold
    cout << "Enter salary threshold: ";
    cin >> salary_threshold;

    // Open data.bin for writing
    ofstream outfile;
    outfile.open("data.bin", ios::binary);

    // Write employees whose salary is greater than threshold to data.bin
    for (i = 0; i < 100; i++) {
        if (employee[i].name == "fin") {
            break;
        }
        if (employee[i].salary > salary_threshold) {
            outfile.write((char*)&employee[i], sizeof(Employee));
            avg_salary += employee[i].salary;
            count++;
        }
    }

    outfile.close();
 
    // Calculate and display average salary of employees in data.bin
    if (count > 0) {
        avg_salary = avg_salary / count;
        cout << "Average salary of employees in data.bin: " << avg_salary << endl;
    }
    else {
        cout << "No employees found in data.bin with salary greater than threshold." << endl;
    }

    return 0;
}
