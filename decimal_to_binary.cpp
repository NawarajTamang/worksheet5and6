#include<iostream>
using namespace std;
#define MAX_SIZE 20
int binary_stack[MAX_SIZE];
int top=-1;
void push_binary(int);
int pop_binary();

void decimal_to_binary(int decimal_num)
{
    int remainder;
    while (decimal_num!=0)
    {
        remainder=decimal_num%2;
        push_binary(remainder);
        decimal_num=decimal_num/2;
    }
    cout<<"Push sucessfully";
    while (top != -1)
    {
        cout<<pop_binary();
    }
}

void push_binary(int remainder)
{
    if(top==MAX_SIZE-1)
    {
        cout<<"Stack is Overflow\n";
        exit(1);
    }
    else
    top=top+1;
    binary_stack[top]=remainder;
}

int pop_binary()
{
    int value;
    value=binary_stack[top];
    top--;
    return value;
}

int main()
{
    int decimal_num;
    cout<<"Enter Decimal number: ";
    cin>>decimal_num;
    decimal_to_binary(decimal_num);

    return 0;
}
