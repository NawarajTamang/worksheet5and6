/*
Define the Employee structure with members: name (use an array of 100 characters), tax
number, and salary. write a program that uses this type to read the data of 100 employess and
store them in an array of such structures. If the user enters fin for name, the data insertion
should terminate and the program should write the structures in the test.bin binary file.
*/
#include<iostream>
#include<fstream>
#include<string>
using namespace std;
struct Employee
{
    string name;
    int tax_number;
    int salary;
};
int main()
{
    Employee employee[100];
    int i; 
    for(i=0; i<100; i++)
    {
        cout<<"Enter the name: ";
        cin>>employee[i].name;
        if(employee[i].name=="fin")
        {
            break;
        }
        cout<<"Enter tax_number: ";
        cin>>employee[i].tax_number;
        cout<<"Enter salary: ";
        cin>>employee[i].salary;
    }
    ofstream outfile;
    outfile.open("test.bin", ios::binary);
    outfile.write((char*)&employee, sizeof(employee));
    outfile.close();

    return 0;
}
